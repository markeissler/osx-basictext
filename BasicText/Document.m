//
//  Document.m
//  BasicText
//
//  Created by Mark Eissler on 7/1/16.
//  Copyright © 2016 Mark Eissler. All rights reserved.
//

#import "Document.h"

#pragma mark - Konstants

@interface Document ()

@end

@implementation Document

#pragma mark - Class Methods

+ (BOOL)autosavesInPlace
{
  return NO;
}

#pragma mark - Lifecycle

- (instancetype)init {
    self = [super init];
    if (self)
    {
      // perform additional init work
    }
    return self;
}

- (void)awakeFromNib
{
}

- (NSString *)windowNibName
{
  // Override returning the nib file name of the document
  // If you need to use a subclass of NSWindowController or if your document
  // supports multiple NSWindowControllers, you should remove this method and
  // override -makeWindowControllers instead.
  return @"Document";
}

#pragma mark - Custom Accessors
#pragma mark - IBActions
#pragma mark - Gesture handlers
#pragma mark - Notification handlers

#pragma mark - Public

- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError
{
  NSData *data = nil;
  NSDictionary *attributes = nil;

  if ([typeName isEqualToString:@"com.markeissler.basictext-txtdoc"])
  {
    attributes = @{
      NSDocumentTypeDocumentAttribute: NSPlainTextDocumentType
    };
  } else if ([typeName isEqualToString:@"com.markeissler.basictext-rtfdoc"]) {
    attributes = @{
      NSDocumentTypeDocumentAttribute: NSRTFTextDocumentType
    };
  }
  
  data = [self.textView.textStorage dataFromRange:NSMakeRange(0, self.textView.textStorage.length) documentAttributes:attributes error:outError];

  if (!data && outError) {
    *outError = [NSError errorWithDomain:NSCocoaErrorDomain code:NSFileWriteUnknownError userInfo:nil];
  }

  return data;
}

- (BOOL)readFromData:(NSData *)data ofType:(NSString *)typeName error:(NSError **)outError {

  NSAttributedString *contentString = nil;
  NSDictionary *options = nil;
  NSDictionary *attributes = nil;

  if ([typeName isEqualToString:@"com.markeissler.basictext-txtdoc"])
  {
    options = @{
      NSDocumentTypeDocumentAttribute: NSPlainTextDocumentType
    };
  } else if ([typeName isEqualToString:@"com.markeissler.basictext-rtfdoc"]) {
    options = @{
      NSDocumentTypeDocumentAttribute: NSRTFTextDocumentType
    };
  } else {
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSArray *supportedTypes = [infoDict valueForKeyPath:@"CFBundleDocumentTypes.@unionOfArrays.CFBundleTypeExtensions"];
    NSString *supportedTypesString = [supportedTypes componentsJoinedByString:@", "];
    NSString *recoverySuggestion = [NSString stringWithFormat:@"Try one of the supported types: %@", supportedTypesString];
    NSDictionary *userInfoDict = @{
      NSLocalizedDescriptionKey: NSLocalizedString(@"Operation was unsuccessful.", nil),
      NSLocalizedFailureReasonErrorKey: NSLocalizedString(@"File type not supported.", nil),
      NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(recoverySuggestion, nil)
    };
    
    *outError = [NSError errorWithDomain:NSCocoaErrorDomain code:NSFileReadUnknownError userInfo:userInfoDict];

    return NO;
  }

  contentString = [[NSAttributedString alloc] initWithData:data options:options documentAttributes:&attributes error:outError];
  if (!contentString && outError) {
    *outError = [NSError errorWithDomain:NSCocoaErrorDomain code:NSFileReadUnknownError userInfo:nil];

    return NO;
  }
  
  if (contentString) {
    dispatch_async(dispatch_get_main_queue(), ^{
      [self.textView.textStorage setAttributedString: contentString];
    });
  }

  return YES;
}

#pragma mark - Private
#pragma mark - Protocol conformance
#pragma mark - XyzDelegate (insert)
#pragma mark - NSCopying
#pragma mark - NSObject
#pragma mark - ...


@end
