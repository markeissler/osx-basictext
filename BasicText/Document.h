//
//  Document.h
//  BasicText
//
//  Created by Mark Eissler on 7/1/16.
//  Copyright © 2016 Mark Eissler. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#pragma mark - Konstants (extern)

@interface Document : NSDocument

// text editor view
@property (readwrite, strong, atomic) IBOutlet NSTextView *textView;

#pragma mark - Class methods
#pragma mark - Lifecycle
#pragma mark - Custom Accessors
#pragma mark - IBActions
#pragma mark - Public

@end

