//
//  main.m
//  BasicText
//
//  Created by Mark Eissler on 7/1/16.
//  Copyright © 2016 Mark Eissler. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
  return NSApplicationMain(argc, argv);
}
