//
//  MOETextView.m
//  BasicText
//
//  Created by Mark Eissler on 7/1/16.
//  Copyright © 2016 Mark Eissler. All rights reserved.
//

#import "MOETextView.h"

#pragma mark - Konstants

// default placeholder text
NSString * const kMOE_BT_DefaultTextViewPlaceholderText = @"Type something here";

@interface MOETextView ()

// private methods
- (void)initCommon;

@end

@implementation MOETextView

#pragma mark - Class Methods

#pragma mark - Lifecycle

- (instancetype)initWithFrame:(NSRect)frameRect textContainer:(NSTextContainer *)container
{
  self = [super initWithFrame:frameRect textContainer:container];
  
  if (self) {
    [self initCommon];
  }
  
  return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
  self = [super initWithCoder:coder];
  
  if (self) {
    [self initCommon];
  }
  
  return self;
}

- (void)initCommon
{
  if (self)
  {
    NSColor *textColor = [NSColor grayColor];
    NSDictionary *attributedStringDictionary = @{
      NSForegroundColorAttributeName: textColor
    };
    
    _attributedPlaceholderString = [[NSAttributedString alloc] initWithString:kMOE_BT_DefaultTextViewPlaceholderText attributes:attributedStringDictionary];

  } else {
    @throw [NSException exceptionWithName:@"IllegalStateException"
              reason:[NSString stringWithFormat:@"Can't call initCommon on nil object for class: %@",
                        [self class]]
              userInfo:nil];
  }

  return;
}

- (void)drawRect:(NSRect)rect
{
  [super drawRect:rect];
  
  if (self.string.length == 0) {
    CGFloat textContainerPadding = self.textContainer.lineFragmentPadding;
    [_attributedPlaceholderString drawAtPoint:NSMakePoint(textContainerPadding, 0.0f)];
  }

  return;
}

- (BOOL)becomeFirstResponder
{
  [self setNeedsDisplay:YES];
  return [super becomeFirstResponder];
}

- (BOOL)resignFirstResponder
{
  [self setNeedsDisplay:YES];
  return [super resignFirstResponder];
}

#pragma mark - Custom Accessors
#pragma mark - IBActions
#pragma mark - Gesture handlers
#pragma mark - Notification handlers
#pragma mark - Public
#pragma mark - Private
#pragma mark - Protocol conformance
#pragma mark - XyzDelegate (insert)
#pragma mark - NSCopying
#pragma mark - NSObject
#pragma mark - ...

@end
