//
//  MOETextView.h
//  BasicText
//
//  Created by Mark Eissler on 7/1/16.
//  Copyright © 2016 Mark Eissler. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#pragma mark - Konstants (extern)

// default placeholder text
extern NSString * const kMOE_BT_DefaultTextViewPlaceholderText;

/**
 *  The MOETextView class extends NSTextView to add placeholder text when the
 *  content is empty.
 */
@interface MOETextView : NSTextView
{
  @protected
  __strong NSAttributedString *_attributedPlaceholderString;
}

#pragma mark - Class methods
#pragma mark - Lifecycle
#pragma mark - Custom Accessors
#pragma mark - IBActions
#pragma mark - Public

@end
