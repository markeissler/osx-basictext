//
//  Macros.h
//  BasicText
//
//  Created by Mark Eissler on 8/5/15.
//  Copyright (c) 2015 Mark Eissler. All rights reserved.
//

#ifndef BasicText_Macros_h
#define BasicText_Macros_h

/**
 *  @name Macros
 *  @{
 *
 *  @cond Macros Group - beg @endcond
 */
#pragma mark - Macros

#if DEBUG || DOXYGEN
  /**
   *  @def LOG_SELECTOR()
   *  A logging macro that prints out the Class name and Selector.
   */
  #define LOG_SELECTOR() \
    NSLog(@"%@::%@", NSStringFromClass([self class]), \
  NSStringFromSelector(_cmd))
  /**
   *  @def LOG_PROGRESS(m, ...)
   *  A logging macro that prints out the Class name, Selector, variables args.
   */
  #define LOG_PROGRESS(m, ...) NSLog(@"%@::%@ - %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [NSString stringWithFormat:@"%@%@", m, [@[__VA_ARGS__] componentsJoinedByString:@""]])
  /**
   *  @def LOG_ERROR(m, ...)
   *  A logging macro that prints out the Class name, Selector, variables args.
   */
  #define LOG_ERROR(m, ...) NSLog(@"%@::%@ - %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [NSString stringWithFormat:@"%@%@", m, [@[__VA_ARGS__] componentsJoinedByString:@""]])
  /**
   *  @def LOG_DUMP(m, ...)
   *  A logging macro that prints out the Class name, Selector, variables args.
   */
  #define LOG_DUMP(m, ...) NSLog(@"%@::%@ - %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), [NSString stringWithFormat:@"%@%@", m, [@[__VA_ARGS__] componentsJoinedByString:@""]])
  /**
   *  @def LOG_INFO(m, ...)
   *  A logging macro that prints out the variables args.
   */
  #define LOG_INFO(m, ...) NSLog(@"%@", [NSString stringWithFormat:@"%@%@", m, [@[__VA_ARGS__] componentsJoinedByString:@""]])
#else
  #define LOG_SELECTOR() do { } while (0)
  #define LOG_PROGRESS(m, ...) do { } while (0)
  #define LOG_ERROR(m, ...) do { } while (0)
  #define LOG_DUMP(m, ...) do { } while (0)
  #define LOG_INFO(m, ...) do { } while (0)
#endif

#define THROW_MALLOC_FAIL() \
  @throw [NSException exceptionWithName:@"MallocException" \
            reason:[NSString stringWithFormat:@"Failed to allocate memory in: %@", \
                      NSStringFromSelector(_cmd)] \
            userInfo:nil];

#endif
