//
//  FileHelpersTests.m
//  BasicText
//
//  Created by Mark Eissler on 7/13/16.
//  Copyright © 2016 Mark Eissler. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FileHelpers.h"

@interface FileHelpersTests : XCTestCase

@property (copy) NSString *tempFilePathPrefix;

@end

@implementation FileHelpersTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _tempFilePathPrefix = @"BT";
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    _tempFilePathPrefix = nil;
  
    [super tearDown];
}

#pragma mark - Class methods

- (void)testPathForTemporaryFileWithPrefixReturnsString
{
  NSString *tempPathString = [FileHelpers pathForTemporaryFileWithPrefix:_tempFilePathPrefix];
  
  XCTAssertTrue(tempPathString.length > 0, @"Class method pathForTemporaryFileWithPrefix failed to return a string");
}

- (void)testPathForTemporaryFileWithPrefixReturnsStringWithBasenamePrefix
{
  NSString *tempPathString = [FileHelpers pathForTemporaryFileWithPrefix:_tempFilePathPrefix];
  NSString *lastPathComponent = tempPathString.lastPathComponent;
  NSRange prefixLocation = [lastPathComponent rangeOfString:_tempFilePathPrefix options:NSAnchoredSearch];
  
  XCTAssertTrue(prefixLocation.location != NSNotFound, @"Class method pathForTemporaryFileWithPrefix failed to add prefix");
}

- (void)testFileURLForTemporaryFileWithPrefixReturnsURLReturnsURL
{
  NSURL *tempPathURL = [FileHelpers fileURLForTemporaryFileWithPrefix:_tempFilePathPrefix];
  
  XCTAssertTrue(tempPathURL.path.length > 0, @"Class method fileURLForTemporaryFileWithPrefix failed to return a url");
}

- (void)testFileURLForTemporaryFileWithPrefixReturnsURLWithBasenamePrefix
{
  NSURL *tempPathURL = [FileHelpers fileURLForTemporaryFileWithPrefix:_tempFilePathPrefix];
  NSString *lastPathComponent = tempPathURL.path.lastPathComponent;
  NSRange prefixLocation = [lastPathComponent rangeOfString:_tempFilePathPrefix options:NSAnchoredSearch];

  XCTAssertTrue(prefixLocation.location != NSNotFound, @"Class method fileURLForTemporaryFileWithPrefix failed to add prefix");
}

@end
