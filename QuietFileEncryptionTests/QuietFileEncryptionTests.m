//
//  QuietFileEncryptionTests.m
//  QuietFileEncryptionTests
//
//  Created by Mark Eissler on 7/12/16.
//  Copyright © 2016 Mark Eissler. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "QuietFileEncryption.h"
#import "Macros.h"

@interface QuietFileEncryptionTests : XCTestCase

@property (strong) NSData *stringData;
@property (strong) NSData *stringDataEncrypted;
@property (strong) NSData *stringDataEncryptedWithMarker;

@end

@implementation QuietFileEncryptionTests

- (void)setUp {
  [super setUp];
  // Put setup code here. This method is called before the invocation of each test method in the class.
  _stringData = [@"hello" dataUsingEncoding:NSUTF8StringEncoding];
  _stringDataEncrypted = [NSData dataWithBytes:"KFOOL" length:5];
  
  // build encrypted data as bytes
  unsigned long markerLength = strlen(kMOE_EncryptionMarker);
  char *encryptedDataBytes = (char *)malloc(sizeof(char)*(markerLength+1));
  if (!encryptedDataBytes) {
    THROW_MALLOC_FAIL();
  }
  memcpy(encryptedDataBytes, kMOE_EncryptionMarker, markerLength);
  memcpy(encryptedDataBytes+markerLength, _stringDataEncrypted.bytes, _stringDataEncrypted.length);
  
  // convert to NSData
  _stringDataEncryptedWithMarker = [NSData dataWithBytes:encryptedDataBytes length:13];
  
  free(encryptedDataBytes);
}

- (void)tearDown {
  // Put teardown code here. This method is called after the invocation of each test method in the class.
  _stringData = nil;
  _stringDataEncrypted = nil;
  _stringDataEncryptedWithMarker = nil;

  [super tearDown];
}

#pragma mark - Test class methods

- (void)testClassMethodEncryptData
{
  // encrypt input
  NSData *encryptedData = [QuietFileEncryption encryptData:_stringData];
  
  // test!
  XCTAssertEqualObjects(encryptedData, _stringDataEncryptedWithMarker, @"Encryption failed");
}

- (void)testClassMethodDecryptData
{
  // decrypt input
  NSData *decryptedData = [QuietFileEncryption decryptData:_stringDataEncryptedWithMarker];

  // test!
  XCTAssertEqualObjects(decryptedData, _stringData, @"Decryption failed");
}

@end
