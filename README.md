# OSX BasicText
The __BasicText__ text editor is an [NSDocument](https://developer.apple.com/library//mac/documentation/Cocoa/Reference/ApplicationKit/Classes/NSDocument_Class/index.html)-based app that reads and writes plain text files (with the `.btxt` extension) and rich text format files (with the `.brtf` extension). The extensions were chosen to convey that these file *may be* encrypted. The __BasicText__ text editor can read unencrypted files as well and will search for a specific 8-byte marker at the head of file data to determine if the file is encrypted.

>NOTE: BasicText does not produce unencrypted files as long as internal encrypt methods are available, the BasicFileEncryption library has been linked and enabled, or transparent support (via QuietFileEncryption) has been linked.

### Building
There is no particular magic for building this project. In Xcode, just select the __BasicText__ target and build. Both the application and library should build. The configured build will link the `libQuietFileEncryption.dylib` library into the project.

### Running
Launch the application by clicking on the `Build and Run` button, using the keyboard shortcut `Cmd-R` or by selecting `Run` from the `Product` menu.

#### Plain Text
Create a new file with `Cmd-N`. Type or cut and paste plain text. Then select `Cmd-S` to save and make sure that `BasicText Plain Text Document` has been selected.

#### Rich text
Create a new file with `Cmd-N`. Type or cut and paste plain text. Then select `Cmd-S` to save and make sure that `BasicText Rich Text Document` has been selected.

>NOTE: A good source of RTF text is Xcode. Just copy and paste source code from Xcode into BasicText.

### Verifying Data
Both the `.btxt` and `.brtf` files are plain-text files. You can verify produced file contents from the command line using your favorite editor or simply the `cat(1)` command:

```sh
	>cat -v my_saved_file.btxt
	[!BT-XORWKJPJPBWFPWLEFM@QZSWJLMNBQHFQ
```
An 8-byte marker consisting of the character sequence `[!BT-XOR` is inserted ahead of all encrypted data to facilitate the detection of an encrypted file vs a file that was saved without encryption (when encryption/decryption has been disabled at compile time).

>NOTE: It is imperative that you use the `-v` flag with the cat(1) command.

### Encryption/Decryption
The [simple XOR cipher](https://en.wikipedia.org/wiki/XOR_cipher) is applied to encrypt the data and is applied again to decrypt the data.

It's possible to enable encryption/decryption in __BasicText__ in two ways:

1. Linked Library Code Injection (Transparent)
2. Dynamic Linked Library Insertion and Code Injection (Transparent)

#### Linked Library Code Injection (Transparent)
To enable encryption/decryption transparently via [swizzled](http://nshipster.com/method-swizzling/) calls make sure `libQuietFileEncryption.dylib` (a dynamic library) is linked into the project:

1. Select the `BasicText` target in Xcode
2. Select the `General` tab
3. Under the `Linked Frameworks and Libraries` section make sure the `libQuietFileEncryption.dylib` appears, if it doesn't, click on the `+` button to add it.

![Add lib quiet](./imgs/add_libQuiet.png)

Rebuild the app.

With this technique, the objective-c class dispatch table is updated to map a substitute method implementation to a target selector while also preserving the the original method implementation under a new selector name. When the original methods is called control is passed to the substitute method--the original method is called by the substitute.

Remapping of method implementations takes places when the library is loaded.

##### Logging
It is possible to verify that code injection is functioning by viewing the console in Xcode:

![Swizzle log](./imgs/swizzle_log.png)

From this log we can see that the dynamic library is loaded, followed by the interception of a call to the `Document::readFromData:ofType:error:` method. Because the method is substituted by leveraging a category on `NSDocument` the `+load` method is called for that class. Our subclass, `Document`, is targeted specifically by resolving the class object by name.

Swizzling is successfully at work here because the substitute method is called with the original method's selector, yet our substitute has a hard-coded value added to its logging statement: `NSDocument+XOR_Encryption.h!`.

#### Dynamic Linked Library Insertion and Code Injection (Transparent)
It is also possible to manipulate the objective-c runtime in order to add the capability of transparent encryption to an executable without specifying linking as part of the build process. That is, we can _inject_ the dynamic library at run time 

With this technique we leverage swizzling as described in the previous section but we skip the linking step in our build configuration. Instead, the dynamic library is specified immediately before runtime. __This technique provides a way to inject code into almost any existing application.__

Make sure the `libQuietFileEncryption.dylib` library is not linked into the project:

1. Select the `BasicText` target in Xcode
2. Select the `General` tab
3. Under the `Linked Frameworks and Libraries` section make sure the `libQuietFileEncryption.dylib` does not appear, if it does, select the line and click on the `-` button to remove it.

![Remove all libs](./imgs/remove_libs.png)

Rebuild the app.

Run the app by launching it from the command line using the following command line arguments:

```sh
	>DYLD_INSERT_LIBRARIES="./Build/Products/Debug/libQuietFileEncryption.dylib" \
	  ./Build/Products/Debug/BasicText.app/Contents/MacOS/BasicText
```

While this method makes it possible to link in our library to almost any application and enable transparent encryption/decryption, there are some caveats to this method:

1. The target must be an [NSDocument](https://developer.apple.com/library//mac/documentation/Cocoa/Reference/ApplicationKit/Classes/NSDocument_Class/index.html)-based app.
2. The target must make calls to the same methods that we are swizzling (see __Swizzled Methods__ below).
2. The target must not be protected by System Integrity Protection.
3. If the target has been signed, it may have been assigned certain entitlements that disable `DYLD_` environment variables at runtime.
4. If the target executable has been assigned `setuid` or `setgid` permissions then the dynamic linker will ignore `DYLD_` environment variables set at runtime.

##### Example Third-party Apps That Work
Third-party apps that have been shown to work with __Dynamic Linked Library Insertion and Code Injection__ are:

1. [TexShop](http://www.texshop.org) - TeX editor for MacOS. Version 3.18 was used for this project but this should work with the current release as well. Both reading and writing are supported. The produced files are text format and easily opened and viewed from the terminal.
2. [BibDesk](http://bibdesk.sourceforge.net/) - Mac Bibliography Manager. Version 1.6.6 (the latest release) was used for this project. Both reading and writing are supported. The produced files are binary format but you can run `strings(1)` on the resulting file and will notice the familiar __BasicText__ Encryption Detection with a Marker in the output.

To try this out with one of the above applications issue the following command from within the project:

```sh
	>DYLD_INSERT_LIBRARIES="./Build/Products/Debug/libQuietFileEncryption.dylib" \
	  /Applications/TeX/TeXShop.app/Contents/MacOS/TeXShop
```

Console...

```sh
2016-07-14 11:08:33.389 TeXShop[94333:49172537] NSDocument::load
2016-07-14 11:08:33.398 TeXShop[94333:49172537] className: NSPersistentDocument
2016-07-14 11:08:33.398 TeXShop[94333:49172537] -- skipping injection for NSPersistentDocument (NOT SUPPORTED)
2016-07-14 11:08:33.398 TeXShop[94333:49172537] className: TSDocument
2016-07-14 11:08:33.398 TeXShop[94333:49172537] -- will swizzle for dataOfType:error:
2016-07-14 11:08:33.398 TeXShop[94333:49172537] -- will swizzle for readFromURL:ofType:error:
2016-07-14 11:08:33.556 TeXShop[94333:49172537] TSDocument::readFromURL:ofType:error: - NSDocument+XOR_Encryption.h!
```

For the above run, open the `test.tex` file from the `QuietFileEncryptionExamples/` directory.

![TeX example decrypted](./imgs/example_decrypted_tex.png)

#### Swizzled Methods and Limitations
Transparent decryption is performed by overriding one of the following NSDocument read methods:

1. `readFromData:ofType:error:`
2. `readFromURL:ofType:error:`

Transparent encryption is performed by overriding one of the following NSDocument read methods:

1. `dataOfType:error:` 
2. `writeToURL:ofType:error:`

If the NSDocument subclass target doesn't override either of the above "read" methods, then decryption will not be possible; likewise, if the target doesn't override either of the above "write" methods, then encryption will not be possible. Unless one "read" and one "write" method has been implemented by the target, these operations will be unreliable.

#### Encryption Detection with a Marker
__BasicText__ cannot tell the difference between an encrypted file and an un-encrypted file when reading. That is, files will receive the same file extension (either `.btxt` or `.brtf`) when saved regardless if encryption is enabled. To assist with the detection of encrypted data an 8-byte market is inserted ahead of all encrypted data (as detailed above in *Verifying Data*).

### Additional Exceptions / Limitations

#### Signed Apps
With OSX 10.5 ("Leopard") Apple introduced a security technology known as ["Code Signing"](https://developer.apple.com/library/mac/documentation/Security/Conceptual/CodeSigningGuide/Introduction/Introduction.html) which prevents the modification of an application by calculating hashes for application's components including:

- Info.plist
- Executable
- Resources

__TL;DR:__ It is no longer possible to tamper with these components as at runtime their signatures (hash calculations) will be incorrect from those recorded 

#### System Integrity Protection (SIP)
With OSX 10.11 ("El Capitan") Apple introduced a security technology known as [*System Integrity Protection*](https://support.apple.com/en-us/HT204899) which enforces a global security policy that encompasses:

- File system protection
- Runtime protection
- Kernel protection

__TL;DR:__ It is no longer possible to tamper with files or processes that are protected by SIP, nor is it possible to tamper with the kernel. With regard to this project, it is not possible to insert a dynamic library into a SIP controlled application such as `Safari.app` as the dynamic linker has been rigged to ignore `DYLD_` environments variables.

### Remarks
For this project, the possibility of exploiting system vulnerabilities or hacking application files were not considered. While there are techniques to circumvent the security features implemented by Apple, employing those techniques to support a commercially available application would be unethical (at the least).

#### Live Code Injection
Another strategy for code injection is to inject code into a running binary as described by and supported by the [mach_inject project](https://github.com/rentzsch/mach_inject) by Jonathan Rentzsch. Apple is working hard to prevent this type of runtime manipulation for the obvious security issues that are at stake. At this time, `mach_inject` [isn't supported on OSX El Capitan](https://github.com/rentzsch/mach_inject/issues/17) without first disabling SPI.

### Tools
Getting swizzling right takes a lot of debugging, that's for sure! The following tools were used to assist with the effort:

- [lldb(1)](http://lldb.llvm.org/lldb-gdb.html)
- nm(1)
- otool(1)
- [class-dump](https://github.com/nygard/class-dump)

### References
The following external references and code were consulted for this project:

[Mac Developer Library: Dynamic Library Programming Topics](https://developer.apple.com/library/mac/documentation/DeveloperTools/Conceptual/DynamicLibraries/100-Articles/UsingDynamicLibraries.html#//apple_ref/doc/uid/TP40002182-SW10)

[mach_inject project](https://github.com/rentzsch/mach_inject) - Interprocess code injection

[Marc's Realm: Extending existing classes (Method Swizzling)](http://darkdust.net/index.php/writings/objective-c/method-swizzling)

[Mike Ash: Friday Q&A 2010-01-29: Method Replacement for Fun and Profit](https://www.mikeash.com/pyblog/friday-qa-2010-01-29-method-replacement-for-fun-and-profit.html)

[Mike Ash: Friday Q&A 2009-01-30: Code Injection](https://www.mikeash.com/pyblog/friday-qa-2009-01-30-code-injection.html)

[NS Hipster: Method Swizzling](http://nshipster.com/method-swizzling/)

[objc.io: Inside Code Signing](https://www.objc.io/issues/17-security/inside-code-signing/)

[stackexchange: What is the "rootless" feature in El Capitan, really?](http://apple.stackexchange.com/questions/193368/what-is-the-rootless-feature-in-el-capitan-really)

[stackoverflow: Objective-C detect if class overrides inherited method](http://stackoverflow.com/questions/17147203/objective-c-detect-if-class-overrides-inherited-method)

[XOR cipher](https://en.wikipedia.org/wiki/XOR_cipher)