
1.0.3 / 2016-08-12
==================

  * Reconfigure build schemes.

v1.0.2 / 2016-07-14
===================

  * Add Tools section to README.
  * Fix for swizzling across multiple subclasses.
  * Fix Xcode linter complaints about nullability hints.
  * Fix example bib file.

v1.0.1 / 2016-07-14
===================

  * Add run instructions for TeXShop.
  * Add Building and Running sections to README.
  * Add to References section in README.
  * Add note on using -v flag with cat(1) command.
  * Add QuietFileEncryption example files.
  * Fix header file search path for QuietFileEncryption target.
  * Add comment on Live Code Injection strategy.

v1.0.0 / 2016-07-13
===================

  * Updated README file for 1.0.0.
  * Clean up formatting. Remove cruft.
  * Remove old swizzleClass method.
  * Improve swizzle reliability via selective swizzling.
  * Use correct macro for logging progress.
  * Add support for swizzling writeToURL and readFromURL.

v0.9.0 / 2016-07-12
===================

  * Update README file for 0.9.0.
  * Refactor tests.
  * Add unit tests for QuietFileEncryption.
  * Fix copy bytes in QuietFileEncryption.
  * Add malloc check in QuietFileEncryption.
  * Strip out BasicTextEncryption scheme and test target.
  * Strip out internal encryption/decryption support.

v0.8.0 / 2016-07-11
===================

  * Add README file for 0.8.0.
  * Add support for internal method, external lib, and transparent external lib file encryption and decryption.
  * Implement encrypt and decrypt internally.
  * Add support to save and read btxt and brtf files.
  * Add MOETextView to support placeholder text.
  * Add maintenance files.
  * Initial Commit
