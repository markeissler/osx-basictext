//
//  NSDocument+XOR_Encryption.m
//  BasicText
//
//  Created by Mark Eissler on 7/7/16.
//  Copyright © 2016 Mark Eissler. All rights reserved.
//

#import "NSDocument+XOR_Encryption.h"
#import <objc/runtime.h>
#import "QuietFileEncryption.h"
#import "Helpers/FileHelpers.h"
#import "Macros.h"

#pragma mark - Konstants

NSString * const kMOE_TemporaryFilePrefix = @"BT";

@interface NSDocument (XOR_Encryption_private)

+ (void)swizzleClass:(Class)class withSelector:(SEL)newSelector forSelector:(SEL)oldSelector;

@end

@implementation NSDocument (XOR_Encryption)

#pragma mark - Class Methods

#pragma mark - Lifecycle

+ (void)load
{
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    LOG_SELECTOR();
    
    // check classess
    int numClasses = objc_getClassList(NULL, 0);
    Class * classes = NULL;
    
    if (numClasses > 0 )
    {
      // only swizzle methods that have been overridden from the parent
      //
      // NSDocument implements defaults for dataOfType:error: and
      // readFromData:ofType:error: but the defaults throw exceptions.
      // Not helpful! We need to check if our target has overridden
      // the inherited methods.
      //
      SEL selectorForDataOfTypeError = @selector(dataOfType:error:);
      SEL selectorForWriteToURLOfTypeError = @selector(writeToURL:ofType:error:);
      SEL selectorForReadFromDataOfTypeError = @selector(readFromData:ofType:error:);
      SEL selectorForReadFromURLOfTypeError = @selector(readFromURL:ofType:error:);
    
      // injection unsafe for class
      //
      // If we can't find both read/write methods to swizzle, then we won't
      // be able to perform both read/write for the target class!
      //
      BOOL readSwizzleSuccess;
      BOOL writeSwizzleSuccess;
    
      classes = (Class *)malloc(sizeof(Class) * numClasses);
      numClasses = objc_getClassList(classes, numClasses);
      for(int i = 0; i < numClasses; i++)
      {
        readSwizzleSuccess = NO;
        writeSwizzleSuccess = NO;
        Class classObj = *(classes+i);
        
        if (class_getSuperclass(classObj) == [NSDocument class])
        {
          const char *className = class_getName(classObj);
          LOG_INFO(@"className: ", [NSString stringWithCString:className encoding:NSUTF8StringEncoding]);
          
          if (strcmp(className, "NSPersistentDocument") == 0) {
            LOG_INFO(@"-- skipping injection for NSPersistentDocument (NOT SUPPORTED)");
            continue;
          }

          // only replace one write method and one read method
          //
          // Otherwise we will end up running through encrypt or decrypt twice!
          // Preference is to replace the data methods because they are called
          // by the URL methods.
          //
          if (method_getImplementation(class_getInstanceMethod(classObj, selectorForDataOfTypeError))
            != method_getImplementation(class_getInstanceMethod([NSDocument class], selectorForDataOfTypeError)))
          {
            LOG_INFO(@"-- will swizzle for dataOfType:error:");
            [self swizzleClass:classObj withSelector:@selector(swizzled_dataOfType:error:) forSelector:selectorForDataOfTypeError];
            writeSwizzleSuccess = YES;
          }
          else if (method_getImplementation(class_getInstanceMethod(classObj, selectorForWriteToURLOfTypeError))
            != method_getImplementation(class_getInstanceMethod([NSDocument class], selectorForWriteToURLOfTypeError)))
          {
            LOG_INFO(@"-- will swizzle for writeToURL:ofType:error:");
            [self swizzleClass:classObj withSelector:@selector(swizzled_writeToURL:ofType:error:) forSelector:selectorForWriteToURLOfTypeError];
            writeSwizzleSuccess = YES;
          }
          else
          {
            LOG_INFO(@"-- skipping *write* injection for ", [NSString stringWithCString:className encoding:NSUTF8StringEncoding]);
          }
          
          if (method_getImplementation(class_getInstanceMethod(classObj, selectorForReadFromDataOfTypeError))
            != method_getImplementation(class_getInstanceMethod([NSDocument class], selectorForReadFromDataOfTypeError)))
          {
            LOG_INFO(@"-- will swizzle for readFromData:ofType:error:");
            [self swizzleClass:classObj withSelector:@selector(swizzled_readFromData:ofType:error:) forSelector:selectorForReadFromDataOfTypeError];
            readSwizzleSuccess = YES;
          }
          else if (method_getImplementation(class_getInstanceMethod(classObj, selectorForReadFromURLOfTypeError))
            != method_getImplementation(class_getInstanceMethod([NSDocument class], selectorForReadFromURLOfTypeError)))
          {
            LOG_INFO(@"-- will swizzle for readFromURL:ofType:error:");
            [self swizzleClass:classObj withSelector:@selector(swizzled_readFromURL:ofType:error:) forSelector:selectorForReadFromURLOfTypeError];
            readSwizzleSuccess = YES;
          }
          else
          {
            LOG_INFO(@"-- skipping *read* injection for ", [NSString stringWithCString:className encoding:NSUTF8StringEncoding]);

          }
          
          if (!(readSwizzleSuccess && writeSwizzleSuccess)) {
            LOG_INFO(@"!! warning: will not be able to read/write reliably");
          }
        }
      }
      free(classes);
    }
  });
}

#pragma mark - Custom Accessors
#pragma mark - IBActions
#pragma mark - Gesture handlers
#pragma mark - Notification handlers

#pragma mark - Public

- (NSData * _Nullable)swizzled_dataOfType:(NSString * _Nonnull)typeName
  error:(NSError * _Nullable __autoreleasing * _Nullable)outError
{
  LOG_PROGRESS(@"NSDocument+XOR_Encryption.h!");
  
  // call original method
  NSData *data = [self swizzled_dataOfType:typeName error:outError];
  
  // encrypt data
  NSData *encryptedData = [QuietFileEncryption encryptData:data];
  data = encryptedData;

  return data;
}

- (BOOL)swizzled_readFromData:(NSData * _Nonnull)data
  ofType:(NSString * _Nonnull)typeName error:(NSError * _Nullable __autoreleasing * _Nullable)outError
{
  LOG_PROGRESS(@"NSDocument+XOR_Encryption.h!");
  
  // decrypt data
  NSData *decryptedData = [QuietFileEncryption decryptData:data];
  data = decryptedData;
  
  // call original method with decrypted data!
  return [self swizzled_readFromData:data ofType:typeName error:outError];
}

- (BOOL)swizzled_writeToURL:(NSURL * _Nonnull)url
  ofType:(NSString * _Nonnull)typeName error:(NSError * _Nullable __autoreleasing * _Nullable)outError
{
  LOG_PROGRESS(@"NSDocument+XOR_Encryption.h!");
  
  // call super writeToURL to write data to temp file, read from temp file,
  // create encrypted data, then write to original destination url.
  NSURL *temporaryFileURL = [FileHelpers fileURLForTemporaryFileWithPrefix:kMOE_TemporaryFilePrefix];
  NSError * __autoreleasing *error = nil;
  BOOL fileWriteSuccessful = [self swizzled_writeToURL:temporaryFileURL ofType:typeName error:error];
  if (!fileWriteSuccessful) {
    return NO;
  }
  
  // read temp file
  NSData *decryptedData = [NSData dataWithContentsOfURL:temporaryFileURL];
  if (!decryptedData) {
    return NO;
  }
  
  // remove temp file
  if (![[NSFileManager defaultManager] removeItemAtURL:temporaryFileURL error:error])
  {
    @throw [NSException exceptionWithName:@"FileManagerException"
              reason:[NSString stringWithFormat:@"Failed to remove temporary file: %@",
                        [temporaryFileURL path]]
              userInfo:nil];
  }
  
  // encrypt data
  NSData *encryptedData = [QuietFileEncryption encryptData:decryptedData];
  
  // save encrypted data
  return [encryptedData writeToURL:url atomically:YES];
}

- (BOOL)swizzled_readFromURL:(NSURL * _Nonnull)url
  ofType:(NSString * _Nonnull)typeName error:(NSError * _Nullable __autoreleasing * _Nullable)outError
{
  LOG_PROGRESS(@"NSDocument+XOR_Encryption.h!");
  
  // read from original source url, create decrypted data, write to temp file,
  // then call super readFromURL to read data from temp file.
  NSData *encryptedData = [NSData dataWithContentsOfURL:url];
  if (!encryptedData) {
    return NO;
  }

  // decrypt data and write to temporary file
  NSData *decryptedData = [QuietFileEncryption decryptData:encryptedData];
  NSURL *temporaryFileURL = [FileHelpers fileURLForTemporaryFileWithPrefix:kMOE_TemporaryFilePrefix];
  BOOL fileWriteSuccessful = [decryptedData writeToURL:temporaryFileURL atomically:YES];
  if (!fileWriteSuccessful)
  {
    return NO;
  }
  
  // pass url to original implementation for reading
  BOOL fileReadSuccessful = [self swizzled_readFromURL:temporaryFileURL ofType:typeName error:outError];
  
  // remove temp file
  NSError * __autoreleasing *error = nil;
  if (![[NSFileManager defaultManager] removeItemAtURL:temporaryFileURL error:error])
  {
    @throw [NSException exceptionWithName:@"FileManagerException"
              reason:[NSString stringWithFormat:@"Failed to remove temporary file: %@",
                        [temporaryFileURL path]]
              userInfo:nil];
  }
 
  return fileReadSuccessful;
}

#pragma mark - Private
#pragma mark - Protocol conformance
#pragma mark - XyzDelegate (insert)
#pragma mark - NSCopying
#pragma mark - NSObject
#pragma mark - ...

@end

@implementation NSDocument (XOR_Encryption_private)

+ (void)swizzleClass:(Class)targetClass withSelector:(SEL)newSelector forSelector:(SEL)oldSelector
{
  // always add a copy of the parent class swizzle method to the target class!
  //
  // This is important, because the targetClass (a subclass) won't have its own
  // copy of the swizzle implementation, which means if we have multiple
  // subclasses, the first one will swap its original method with the parent,
  // then the next one will swap that implementation in for its swizzle, and so
  // on. A pain to debug!
  //
  Method parent_swizzledMethod = class_getInstanceMethod([NSDocument class], newSelector);
  class_addMethod(targetClass, newSelector,
    method_getImplementation(parent_swizzledMethod), method_getTypeEncoding(parent_swizzledMethod));
  
  Method targetOriginalMethod = class_getInstanceMethod(targetClass, oldSelector);
  Method targetSwizzledMethod = class_getInstanceMethod(targetClass, newSelector);
  
  if (class_addMethod(targetClass, oldSelector,
    method_getImplementation(targetSwizzledMethod), method_getTypeEncoding(targetSwizzledMethod)))
  {
    class_replaceMethod(targetClass, newSelector,
      method_getImplementation(targetOriginalMethod), method_getTypeEncoding(targetOriginalMethod));
  } else {
    method_exchangeImplementations(targetOriginalMethod, targetSwizzledMethod);
  }
}

@end
