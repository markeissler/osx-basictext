//
//  FileHelpers.h
//  BasicText
//
//  Created by Mark Eissler on 7/13/16.
//  Copyright © 2016 Mark Eissler. All rights reserved.
//

#import <AppKit/AppKit.h>

/**
 *  A collection of file system helper (i.e. convenience) methods.
 */
@interface FileHelpers : NSObject

/**
 *  @brief Generate a file path for a temporary file.
 *
 *  @param prefix NSString to prepend to generated path basename.
 *
 *  @return NSString containing path on success, nil on failure.
 */
+ (NSString *)pathForTemporaryFileWithPrefix:(NSString *)prefix;

/**
 *  @brief Generate a file URL for a temporary file.
 *
 *  The NSURL scheme will be set to <i>file:\/\/\/</i>.
 *
 *  @param prefix NSString to prepend to generated path basename.
 *
 *  @return NSURL containing path on succes, nil on failure.
 */
+ (NSURL *)fileURLForTemporaryFileWithPrefix:(NSString *)prefix;

@end
