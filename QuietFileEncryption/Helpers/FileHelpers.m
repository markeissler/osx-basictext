//
//  FileHelpers.m
//  BasicText
//
//  Created by Mark Eissler on 7/13/16.
//  Copyright © 2016 Mark Eissler. All rights reserved.
//

#import "FileHelpers.h"

@implementation FileHelpers

+ (NSString *)pathForTemporaryFileWithPrefix:(NSString *)prefix
{
  NSString *temporaryPath;
  CFUUIDRef uuidRef;
  CFStringRef uuidStringRef;

  uuidRef = CFUUIDCreate(NULL);
  assert(uuidRef != NULL);

  uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
  assert(uuidStringRef != NULL);

  temporaryPath = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@-%@", prefix, uuidStringRef]];

  CFRelease(uuidStringRef);
  CFRelease(uuidRef);

  return temporaryPath;
}

+ (NSURL *)fileURLForTemporaryFileWithPrefix:(NSString *)prefix
{
  NSString *temporaryFilePath = [self pathForTemporaryFileWithPrefix:prefix];
  
  return [NSURL fileURLWithPath:temporaryFilePath];
}

@end
