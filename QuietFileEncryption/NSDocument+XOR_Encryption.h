//
//  NSDocument+XOR_Encryption.h
//  BasicText
//
//  Created by Mark Eissler on 7/7/16.
//  Copyright © 2016 Mark Eissler. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#pragma mark - Konstants (extern)

@interface NSDocument (XOR_Encryption)

#pragma mark - Class methods
#pragma mark - Lifecycle
#pragma mark - Custom Accessors
#pragma mark - IBActions

#pragma mark - Public

- (NSData * _Nullable)swizzled_dataOfType:(NSString * _Nonnull)typeName
  error:(NSError * _Nullable __autoreleasing * _Nullable)outError;
- (BOOL)swizzled_readFromData:(NSData * _Nonnull)data
  ofType:(NSString * _Nonnull)typeName error:(NSError * _Nullable __autoreleasing * _Nullable)outError;
- (BOOL)swizzled_writeToURL:(NSURL * _Nonnull)url
  ofType:(NSString * _Nonnull)typeName error:(NSError * _Nullable __autoreleasing * _Nullable)outError;
- (BOOL)swizzled_readFromURL:(NSURL * _Nonnull)url
  ofType:(NSString * _Nonnull)typeName error:(NSError * _Nullable __autoreleasing * _Nullable)outError;

@end
