//
//  QuietFileEncryption.m
//  QuietFileEncryption
//
//  Created by Mark Eissler on 7/6/16.
//  Copyright © 2016 Mark Eissler. All rights reserved.
//

#import "QuietFileEncryption.h"
#import <objc/runtime.h>
#import "Macros.h"

#pragma mark - Konstants

NSString * const kMOE_EncryptionKey = @"#";
const char * const kMOE_EncryptionMarker = "[!BT-XOR";

@interface QuietFileEncryption ()

/**
 *  @internal
 *
 *  @brief Encrypt or decrypt data using XOR cipher.
 *
 *  The simple XOR cipher accomplishes encryption by applying the bitwise XOR
 *  operator to each byte using a given key. In this implementation, the key is
 *  a single character value.
 *
 *  This method should be called to encrypt the payload data exclusively, that
 *  is, the data without any additional file markers or headers which should
 *  remainin unencrypted.
 *
 *  The results of this method are not idempotent: data that has not yet been
 *  encrypted will be encrypted, while data that has been encrypted will be
 *  decrypted.
 *
 *  @param data The data to encrypt or decrypt.
 *
 *  @return The encrypted or decrypted data.
 */
+ (NSData *)_mungePayload:(NSData *)data;

@end

@implementation QuietFileEncryption

#pragma mark - Class Methods

+ (NSData *)_mungePayload:(NSData *)data
{
  NSMutableData *mungedPayload = [NSMutableData dataWithLength:data.length];

  // convert encryption key string to bytes
  char *keyBytes = malloc(sizeof(char)*[kMOE_EncryptionKey lengthOfBytesUsingEncoding:NSUTF8StringEncoding]);
  [kMOE_EncryptionKey getBytes:keyBytes maxLength:1 usedLength:NULL
    encoding:NSUTF8StringEncoding options:NSStringEncodingConversionAllowLossy
    range:NSMakeRange(0, 1) remainingRange:NULL];

  __block NSUInteger bytesCopiedIndex = 0;
  [data enumerateByteRangesUsingBlock:^(const void * _Nonnull bytes, NSRange byteRange, BOOL * _Nonnull stop) {
    for (NSUInteger i = 0; i < byteRange.length; i++) {
      char byte = ((const char *)bytes)[i];
      byte = byte ^ keyBytes[0];
      [mungedPayload replaceBytesInRange:NSMakeRange(bytesCopiedIndex, 1) withBytes:&byte];
      bytesCopiedIndex++;
    }
  }];
  if (keyBytes != nil) {
    free(keyBytes);
  }
  
  return mungedPayload;
}

+ (NSData *)encryptData:(NSData *)data
{
  unsigned long markerLength = strlen(kMOE_EncryptionMarker);
  NSMutableData *encryptedData = [NSMutableData dataWithLength:markerLength+data.length];
  
  // add marker
  [encryptedData replaceBytesInRange:NSMakeRange(0, markerLength) withBytes:(const void *)kMOE_EncryptionMarker];
  
  // add encrypted data
  NSData *payload = [QuietFileEncryption _mungePayload:data];
  [encryptedData replaceBytesInRange:NSMakeRange(markerLength, encryptedData.length-markerLength) withBytes:payload.bytes];

  return encryptedData;
}

+ (NSData *)decryptData:(NSData *)data
{
  BOOL isEncryptedData = NO;
  unsigned long markerLength = strlen(kMOE_EncryptionMarker);
  
  // detect marker
  if (data.length >= 8) {
    char *signatureBytes = (char *)malloc(sizeof(char) * (markerLength+1));
    if (!signatureBytes)
    {
      THROW_MALLOC_FAIL();
    }
    memset(signatureBytes, '\0', markerLength+1);
    [data getBytes:signatureBytes range:(NSMakeRange(0, markerLength))];
    if (!strncmp(kMOE_EncryptionMarker, signatureBytes, markerLength)) {
      isEncryptedData = YES;
    }
    free(signatureBytes);
  }

  if (!isEncryptedData) {
    return data;
  }
  
  // strip marker
  NSData *payload = [data subdataWithRange:NSMakeRange(markerLength, data.length-markerLength)];
  NSData *decryptedData = [QuietFileEncryption _mungePayload:payload];
  
  return decryptedData;
}

#pragma mark - Lifecycle
#pragma mark - Custom Accessors
#pragma mark - IBActions
#pragma mark - Gesture handlers
#pragma mark - Notification handlers
#pragma mark - Public
#pragma mark - Private
#pragma mark - Protocol conformance
#pragma mark - XyzDelegate (insert)
#pragma mark - NSCopying
#pragma mark - NSObject
#pragma mark - ...

@end
