//
//  QuietFileEncryption.h
//  QuietFileEncryption
//
//  Created by Mark Eissler on 7/6/16.
//  Copyright © 2016 Mark Eissler. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - Konstants (extern)

/**
 *  Encryption marker is prepended to encrypted data.
 */
extern const char * const kMOE_EncryptionMarker;

/**
 *  @brief The QuietFileEncryption class implements transparent file encryption 
 *  and decryption using the simple XOR cipher.
 *
 *  The simple XOR cipher accomplishes encryption by applying the bitwise XOR
 *  operator to each byte using a given key. In this implementation, the key is
 *  a single character value.
 *
 *  Transparent operation is implement via code injection whereby the following
 *  NSDocument methods are intercepted:
 *    - NSDocument#dataOfType:error:
 *    - NSDocument#readFromData:ofType:error:
 *    .
 *
 *  Files will be encrypted and decrypted provided that the underlying subclass
 *  of NSDocument call the above methods during read/write operations.
 */
@interface QuietFileEncryption : NSObject

#pragma mark - Class methods

/**
 *  @brief Encrypt data.
 *
 *  This method will encrypt data and prepend an encryption method marker ahead
 *  of the encrypted payload.
 *
 *  @param data Reference NSData object containing the data to encrypt.
 *
 *  @return Initialized NSData object on success, containing encrypted data, nil
 *    on failure.
 *
 *  @see #decryptData:
 */
+ (NSData *)encryptData:(NSData *)data;

/**
 *  @brief Decrypt data previously encrypting with the #encryptData: method.
 *
 *  This method will remove the encryption method marker and return the
 *  decrypted data payload.
 *
 *  @param data Reference to NSData object containing the data to decrypt.
 *
 *  @return Initialized NSData object on success, containing decrypted data, nil
 *    on failure.
 *
 *  @see #encryptData:
 */
+ (NSData *)decryptData:(NSData *)data;

#pragma mark - Lifecycle
#pragma mark - Custom Accessors
#pragma mark - IBActions
#pragma mark - Public

@end
